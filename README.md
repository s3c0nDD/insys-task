# insys-task
This is recruitment task for Insys, built in React, based on my boilerplate [yet-another-react-boilerplate](https://github.com/s3c0nDD/yet-another-react-boilerplate).

After cloning do:
```
cd insys-task
yarn install
yarn start 
```

or `yarn build` in case you want to build app.

----
### LICENCE 

[MIT](https://opensource.org/licenses/MIT) 

Copyright © 2017 Witold Janik
