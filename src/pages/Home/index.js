import React, { Component } from 'react';

import FontAwesome from 'react-fontawesome';

import './style.css';

class Home extends Component {
  render() {
    return (
      <div className="Home">
        <div className="Home__header">
          <h1 className="main-header">Marylin Monroe</h1>
          <p className="sub-header"><FontAwesome name="map-marker" /> Poznan, PL</p>
        </div>
        <div className="Home__content">
          <p className="text">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dignissimos eligendi ipsum itaque
            officiis sequi tempore temporibus. At, cum dicta, dolore eum fugiat magnam maxime nesciunt non sint,
            temporibus vitae.
          </p>
          <p className="quote">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad, aliquam cumque distinctio doloribus
            ducimus, eius eos eveniet hic magnam natus nisi non obcaecati provident rerum tenetur ut, veritatis
            voluptate.
          </p>
          <p className="text">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur beatae dolore eligendi
            excepturi, incidunt ipsum nesciunt, placeat quasi soluta ullam voluptatibus! Alias doloribus eaque,
            laboriosam nemo nostrum provident quos.
          </p>
        </div>
      </div>
    );
  }
}

export default Home;