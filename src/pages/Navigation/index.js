import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import FontAwesome from 'react-fontawesome';
import avatar from './images/avatar.jpg';

import './style.css';

class Navigation extends Component {
  render() {
    return (
      <div className="Navigation">

        <div className="Navigation__top" />

        <div className="Navigation__bottom">

          <div className="avatar">
            <img src={avatar} className="avatar--image" alt="marylin avatar" />
          </div>

          <nav className="links">
            <div className="link">
              <NavLink exact activeClassName="active" to="/">
                <FontAwesome name="user" />
              </NavLink>
            </div>
            <div className="link">
              <NavLink activeClassName="active" to="/gallery">
                <FontAwesome name="picture-o" />
              </NavLink>
            </div>
          </nav>

        </div>

      </div>
    );
  }
}

export default Navigation;