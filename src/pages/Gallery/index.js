import React, { Component } from 'react';
import axios from 'axios';

import './style.css';

class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: []
    };
  }

  preparePhotosFromApi = (photos) => {
    let result = [];
    photos.forEach((obj) => {
      let photo = [
        /*thumbnail:*/      obj.media.m,
        /*full_url:*/       obj.link,
        /*title:*/          obj.title
      ];
      result.push(photo);
    });
    return result;
  };

  fetchGallery = () => {
    const self = this,
      query = `/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags=monroe,+marylin`;
    axios.get(query)
      .then((res) => {
        let photos = res.data.items;
        photos.map((elem) => elem.published).sort();  // sort by date
        photos = photos.slice(0, 9);                  // get 9 newest elements
        photos = self.preparePhotosFromApi(photos);
        let newState = Object.assign({}, self.state.photos);
        newState = photos;
        self.setState({photos: newState}/*, () => console.log('state', self.state)*/);
      })
      .catch((err) => {
        console.log(err);
        alert('Something went wrong with gallery add!\n\n' + err);
      });
  };

  componentDidMount() {
    this.fetchGallery();
    // TODO: store photos after first fetch (redux or some other global state object? or maybe push state one lvl up)
  };

  render() {
    let content = (<div></div>);

    if (!(0 < this.state.photos.length)) {
      content = (
        <div className="images">
          {Array.apply(0, Array(9)).map((el, idx) =>
            <div className="image" key={idx}>
              <img className="img-responsive"
                   alt={`marylin${idx+1}`}
                   src="http://placehold.it/220x120"/>
            </div>
          )}
        </div>
      );
    } else {
      content = (
        <div className="images">
          {this.state.photos.map((photo, idx) =>
            <div className="image" key={idx}>
              <a href={photo[1]}
                 target="_blank">
                <img className="img-responsive"
                     alt={photo[2]}
                     src={photo[0]}/>
              </a>
            </div>
          )}
        </div>
      );
    }

    return (
      <div className="Gallery">
        <div className="Gallery__container">
          {content}
        </div>
      </div>
    );
  }
}

export default Gallery;