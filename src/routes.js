import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import Navigation from './pages/Navigation/index';
import Home from './pages/Home/index';
import Gallery from './pages/Gallery/index';

class Routes extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navigation />
          <div className="container">
            <Switch >
              <Route exact path="/" component={Home} />
              <Route path="/gallery" component={Gallery} />
            </Switch>
          </div>
        </div>
      </Router>
    )
  }
}

export default Routes;
